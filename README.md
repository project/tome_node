<strong>Extends <a href="https://www.drupal.org/project/tome" title="Tome Module">Tome Module</a></strong>

Provides Command For Node Generation where nid is argument.

<strong>Usage:</strong>
<code>drush tome:static-export-node-path nid process-count path-count return-json retry-count</code>

where:

<strong>nid: </strong><em>(Required) </em> A comma separated list of node id.
<strong>process-count:</strong><em>(Optional) </em>Limits the number of processes to run concurrently.
<strong>path-count: </strong><em>(Optional) </em>The number of paths to export per process.
<strong>return-json: </strong> <em>(Optional) </em>Whether or not paths that need invoking should be returned as JSON.
<strong>retry-count: </strong><em>(Optional) </em>The number of retry per failed process.
